using App_Db_First.Models;
using Microsoft.AspNetCore.Mvc;

namespace App_Db_First.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly AdventureWorks2019Context _dbcontext;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, 
            AdventureWorks2019Context dbcontext)
        {
            _dbcontext = dbcontext;
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<dynamic> Get()
        {
            return _dbcontext.Vendors.ToList();
        }
    }
}
